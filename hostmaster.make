core = 7.x
api = 2

; Contrib modules
projects[admin_menu][version] = "3.0-rc1"
projects[context][version] = "3.0-beta1"
projects[ctools][version] = "1.0-beta1"
projects[features][version] = "1.0-beta2"
projects[openidadmin][version] = "1.0"

; Libraries
libraries[profiler][download][type] = "get"
libraries[profiler][download][url] = "http://ftp.drupal.org/files/projects/profiler-6.x-2.0-beta1.tar.gz"

