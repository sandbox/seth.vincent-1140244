; this makefile exists for the sole purpose of working around:
; http://drupal.org/node/751242
;
; It was converted from the regular makefile on drupal.org
;
; Logged conversion messages follow. The final converted .make file follows these messages.
; [ok]: 	 Setting version for project 'admin_menu' to '1.6'
; [ok]: 	 Setting version for project 'openidadmin' to '1.2'
; [ok]: 	 Setting version for project 'install_profile_api' to '2.1'
; [ok]: 	 Setting version for project 'jquery_ui' to '1.3'
; [ok]: 	 Setting version for project 'modalframe' to '1.6'
; [warning]: 	 The 'api' make file attribute is not allowed, removing.
; [warning]: 	 The 'libraries' make file attribute is not allowed, removing.
core = 7.2
projects[admin_menu] = 3.0-rc1
projects[openidadmin] = 1.0
; projects[install_profile_api] = 2.1
; projects[jquery_ui] = 1.3
; projects[modalframe] = 1.6
