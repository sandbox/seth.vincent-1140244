<?php

/**
 * Implementation of hook_context_default_contexts().
 */
function hosting_ui_context_default_contexts() {
  $export = array();
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'global';
  $context->description = '';
  $context->tag = 'Hosting';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'user-1' => array(
          'module' => 'user',
          'delta' => 1,
          'region' => 'left',
          'weight' => 0,
        ),
        'user-0' => array(
          'module' => 'user',
          'delta' => 0,
          'region' => 'left',
          'weight' => 1,
        ),
        'system-0' => array(
          'module' => 'system',
          'delta' => 0,
          'region' => 'left',
          'weight' => 2,
        ),
        'hosting-hosting_queues' => array(
          'module' => 'hosting',
          'delta' => 'hosting_queues',
          'region' => 'right',
          'weight' => 0,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Hosting');

  $export['global'] = $context;
  return $export;
}
