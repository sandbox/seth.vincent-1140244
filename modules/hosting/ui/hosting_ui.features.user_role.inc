<?php

/**
 * Implementation of hook_user_default_roles().
 */
function hosting_ui_user_default_roles() {
  $roles = array();

  // Exported role: aegir account manager
  $roles['aegir account manager'] = array(
    'name' => 'aegir account manager',
  );

  // Exported role: aegir client
  $roles['aegir client'] = array(
    'name' => 'aegir client',
  );

  return $roles;
}
