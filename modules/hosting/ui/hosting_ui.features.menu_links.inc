<?php

/**
 * Implementation of hook_menu_default_menu_links().
 */
function hosting_ui_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: primary-links:hosting/platforms
  $menu_links['primary-links:hosting/platforms'] = array(
    'menu_name' => 'primary-links',
    'link_path' => 'hosting/platforms',
    'router_path' => 'hosting/platforms',
    'link_title' => 'Platforms',
    'options' => array(
      'attributes' => array(
        'title' => 'List of platforms',
      ),
    ),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: primary-links:hosting/servers
  $menu_links['primary-links:hosting/servers'] = array(
    'menu_name' => 'primary-links',
    'link_path' => 'hosting/servers',
    'router_path' => 'hosting/servers',
    'link_title' => 'Servers',
    'options' => array(
      'attributes' => array(
        'title' => 'List of servers',
      ),
    ),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: primary-links:hosting/sites
  $menu_links['primary-links:hosting/sites'] = array(
    'menu_name' => 'primary-links',
    'link_path' => 'hosting/sites',
    'router_path' => 'hosting/sites',
    'link_title' => 'Sites',
    'options' => array(
      'attributes' => array(
        'title' => 'Display a list of sites',
      ),
    ),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Platforms');
  t('Servers');
  t('Sites');


  return $menu_links;
}
